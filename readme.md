## Plano de Teste
API Restricao CPF e Simular dados.

Versão 1.0

1. Introdução
Esse documento descreve os requisitos de teste para a API para um melhor entendimento, saber 
a API atende as funcionalidades que foram propostas, desempenho, confiabilidade e segurança. 
A intencao é encontrar algum BUG ou algum comportamento inesperado.

Formato da API : JSON.
Documentacao: Swagger.
Autenticacao: Não tem.
Dados: Não são criptografados e nem autenticada.
Quem consome a API: Apenas desenvolvedores. (API Teste).

2. Tipos de testes não funcionais (performance)
* Teste de Carga 
* Teste de Stress
* Teste de Spike 
* Teste de Endurance
* Teste de Contrato

3. Requisitos para teste
* Fazer o download sources e documentation das dependencias adicionadas
* Swagger: localhost:8080/swagger-ui.html#/

4. Requisicoes desta API
* Restricoes: 
- GET /api/v1/restricoes/{cpf}
* Simulacoes: 
- GET /api/v1/simulacoes
- GET /api/v1/simulacoes/{cpf}
- POST /api/v1/simulacoes
- PUT /api/v1/simulacoes/{cpf}
- DELETE /api/v1/simulacoes/{id}

5. Como executar os testes

GET: Para executar é simples, apenas clicar no botão de play ao lado do teste na class que deseja
se quer executar apenas um teste, se nao, clicar com o botao direito na class e executar tudo.

POST: Apenas alterar os dados do body para as informacoes que deseja criar e executar o teste
clicando no play.

PUT: Executar a requisicao GET, escolher um dado da sua preferencia para alterar.

DELETE: Executar a requisicao GET e escolher um dado de sua preferencia para deletar.

## Erros na aplicacao

1. Os dados que foram diponibilizados dizendo que sao CPF com restrição entao incorretos,
   qualquer CPF que coloco da 204 sem diferenciar se é restrito ou nao. Só irá dar status da casa do 400
   se inserir uma letra ou numero com letra.
2. O DELETE está deixando inserir qualquer ID mesmo nao existindo o ID de um registro e dando sucesso
   na exclusao.







