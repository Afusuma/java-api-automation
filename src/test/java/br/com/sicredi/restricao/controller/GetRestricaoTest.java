package br.com.sicredi.restricao.controller;

import br.com.sicredi.BaseTest;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.when;

@SpringBootTest
public class GetRestricaoTest extends BaseTest {

    @Test
    public void getNaoPossuiRestricao() {
        when().get("/v1/restricoes/11122233344").prettyPrint().equals(204);
    }

    @Test
    public void getPossuiRestricao() {
        when().get("/v1/restricoes/60094146012").prettyPrint().equals(200);
    }
}
