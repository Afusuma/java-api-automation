package br.com.sicredi.simulacao.controller;

import br.com.sicredi.BaseTest;
import br.com.sicredi.dominio.Dados;
import net.minidev.json.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PostSimulacaoTest extends BaseTest {
    public static String nome =  "Amanda";
    public static String emailPadrao = "admin@test.com";

    @Test
    public void postSimulacoesSucesso() {
        Dados dados = new Dados("Patriciaaa", "60094146012", "admin@test.com", "2000", 5, true );
        given()
                .body(dados)
            .when()
                .post("/v1/simulacoes")
            .then()
                .statusCode(equalTo(201));
    }

    @Test
    public void postSimulacoesErro() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("name", nome);
        requestParams.put("cpf", "60094146012");
        requestParams.put("email", emailPadrao);
        requestParams.put("valor", 2000);
        requestParams.put("parcelas", 5);
        requestParams.put("seguro", true);

        given()
                .body(requestParams.toString())
            .when()
                .post("/v1/simulacoes")
            .then()
                .statusCode(equalTo(400));
    }

    @Test
    public void postSimulacoesCPFExistente() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("nome", nome);
        requestParams.put("cpf", "60094146012");
        requestParams.put("email", emailPadrao);
        requestParams.put("valor", 2000);
        requestParams.put("parcelas", 5);
        requestParams.put("seguro", true);

        given()
                .body(requestParams.toString())
            .when()
                .post("/v1/simulacoes")
            .then()
                .statusCode(equalTo(400)).assertThat()
                .body("mensagem", equalTo("CPF duplicado")).log();
    }

}
