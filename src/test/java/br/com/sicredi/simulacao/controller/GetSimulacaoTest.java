package br.com.sicredi.simulacao.controller;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import br.com.sicredi.BaseTest;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class GetSimulacaoTest extends BaseTest {

    @Test
    public void getSimulacoes() {
        when().get("/v1/simulacoes").prettyPrint().equals(200);
    }

    @Test
    public void getSimulacoesPorCPF() {
        when().get("/v1/simulacoes/11122233344").prettyPrint().equals(200);
    }

    @Test
    public void getSimulacoesPorCPFNaoEncontrado() {
        when().get("/v1/simulacoes/600941460123")
                .then().statusCode(equalTo(404));
    }
}