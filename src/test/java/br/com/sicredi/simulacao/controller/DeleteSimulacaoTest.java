package br.com.sicredi.simulacao.controller;

import br.com.sicredi.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class DeleteSimulacaoTest extends BaseTest {
    @Test
    public void deleteSimulacoesPorID() {
        when().delete("/v1/simulacoes/12")
                .then().statusCode(equalTo(200));
    }

    @Test
    public void deleteSimulacoesPorIDInvalido() {
        when().delete("/v1/simulacoes/1010101010")
                .then().statusCode(equalTo(404))
                .body("mensagem", equalTo("Simulação não encontrada" )).log();
    }
}
