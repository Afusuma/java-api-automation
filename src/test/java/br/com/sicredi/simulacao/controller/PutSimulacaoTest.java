package br.com.sicredi.simulacao.controller;

import br.com.sicredi.BaseTest;
import net.minidev.json.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PutSimulacaoTest extends BaseTest {
    public static String nome =  "TESTEEEE2";
    public static String emailPadrao = "admin@test.com";

    @Test
    public void putSimulacoe() {
        JSONObject requestParams = new JSONObject();
        String caminhoCPF = "/v1/simulacoes/66414919004";

        requestParams.put("nome", nome);
        requestParams.put("cpf", "11122233344");
        requestParams.put("email", emailPadrao);
        requestParams.put("valor", 2000);
        requestParams.put("parcelas", 5);
        requestParams.put("seguro", true);

        given()
                .body(requestParams.toString())
            .when()
                .put(caminhoCPF)
            .then()
                .statusCode(equalTo(200));
    }

    @Test
    public void putSimulacoeCPFErrado() {
        JSONObject requestParams = new JSONObject();
        String caminhoCPF = "/simulacoes/66414919004";

        requestParams.put("nome", nome);
        requestParams.put("cpf", "66414919004");
        requestParams.put("email", emailPadrao);
        requestParams.put("valor", 2000);
        requestParams.put("parcelas", 5);
        requestParams.put("seguro", true);

        given()
                .body(requestParams.toString())
            .when()
                .put(caminhoCPF)
            .then()
                .statusCode(equalTo(404))
                .body("mensagem", equalTo("CPF 66414919004 não encontrado")).log();
    }

}
