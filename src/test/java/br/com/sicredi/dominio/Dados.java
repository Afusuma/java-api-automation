package br.com.sicredi.dominio;

import java.math.BigDecimal;

public class Dados {

    private String nome;
    private String cpf;
    private String email;
    private String valor;
    private Integer parcelas;
    private Boolean seguro;

    public Dados(String nome, String cpf, String email, String valor, Integer parcelas, Boolean seguro) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
        this.seguro = seguro;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public String getValor() {
        return valor;
    }

    public Integer getParcelas() {
        return parcelas;
    }

    public Boolean getSeguro() {
        return seguro;
    }
}
